<?php

/**
 * This File is part of the Stream\Common package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

if (function_exists('array_get')) {
    return;
}

/**
 * array_get
 *
 * @param mixed $namespace
 * @param array $array
 * @param string $separator
 * @access
 * @return mixed
 */
function array_get($namespace, array $array, $separator = '.')
{
    $keys = explode($separator, $namespace);

    if (!isset($array[current($keys)])) {
        return;
    }

    while (count($keys) > 0) {
        $array = $array[array_shift($keys)];
    }
    return $array;
}

/**
 * parse a segmented string to an array
 *
 * @param string $namespace
 * @param mixed  $value
 * @param array  $array
 * @param string $separator
 *
 * @return array
 */
function array_set($namespace, $value, array &$array = [], $separator = '.')
{
    $keys = explode($separator, $namespace);
    $key = array_shift($keys);

    if (!count($keys) && $array[$key] = $value) {
        return $array;
    }

    if (!isset($array[$key])) {
        $array[$key] = [];
    }

    return array_set(implode($separator, $keys), $value, $array[$key], $separator);
}

/**
 * array_pluck
 *
 * @param mixed $key
 * @param mixed $array
 * @access
 * @return mixed
 */
function array_pluck($key, array $array)
{
    return array_map(function ($item) use ($key)
    {
        return is_object($item) ? $item->$key : $item[$key];
    }, $array);
}

/**
 * array_zip
 *
 * @access
 * @return mixed
 */
function array_zip()
{
    $args = func_get_args();
    $count = count($args);

    $out = [];

    for ($i = 0; $i < $count; $i++) {
        $out[$i] = array_pluck($i, $args);
    }
    return $out;
}

/**
 * array_max
 *
 * @param array $args
 * @access
 * @return mixed
 */
function array_max(array $args)
{
    uasort($args, function ($a, $b) {
        return count($a) < count($b) ? 1 : -1;
    });
    return count(head($args));
}

/**
 * array_min
 *
 * @param array $args
 * @access
 * @return mixed
 */
function array_min(array $args)
{
    usort($args, function ($a, $b) {
        return count($a) < count($b) ? 1 : -1;
    });
    return count(tail($args));
}

/**
 * array_head
 *
 * @param array $array
 * @access
 * @return mixed
 */
function head(array $array)
{
    return reset($array);
}

/**
 * array_tail
 *
 * @access
 * @return mixed
 */
function tail(array $array)
{
    return end($array);
}

/**
 * array_numeric
 *
 * @param array $array
 *
 * @return boolean
 */
function array_numeric(array $array)
{
    return ctype_digit(implode('', array_keys($array)));
}

/**
 * array_compact
 *
 * @param array $array
 *
 * @return array
 */
function array_compact(array $array)
{
    $out = array_filter($array, function ($item)
    {
        return false !== (bool)$item;
    });
    return array_numeric($out) ? array_values($out) : $out;
}

/**
 * null a given value in case it's an empty string
 *
 * @param mixed $value
 *
 * @return mixed
 */
function clear_value($value)
{
    return ((is_string($value) && 0 === strlen(trim($value))) || is_null($value)) ? null : $value;
}

/**
 * equals
 *
 * @param mixed $a
 * @param mixed $b
 *
 * @return boolean
 */
function equals($value, $comparator)
{
    return $value == $comparator;
}

/**
 * same
 *
 * @param mixed $a
 * @param mixed $b
 * @access
 * @return mixed
 */
function same($value, $comparator)
{
    return  $value === $comparator;
}

/**
 * camelcase notataion
 *
 * convert lowdash to camelcase notation
 *
 * @param mixed $str
 * @access
 * @return mixed
 */
function str_camel_case($str)
{
    return lcfirst(str_camel_case_all($str));
}
/**
 * all camelcase notataion
 *
 * @param string $string
 *
 * @return string
 */
function str_camel_case_all($string)
{
    return str_replace(' ', null, ucwords(str_replace(['-', '_'], ' ', $string)));
}

/**
 * convert camelcase to low dash notation
 *
 * @param string $string
 *
 * @return string
 */
function str_low_dash($string)
{
    return strtolower(preg_replace('/[A-Z]/', '_$0', $string));
}

/**
 * determine if a string starts wiht a given sequence
 *
 * @param string $sequence
 * @param string $string
 *
 * @return boolean
 */
function str_starts_with($sequence, $string)
{
    return 0 === strpos($string, $sequence);
}

/**
 * determine if a string starts wiht a given sequence
 *
 * @param string $sequence
 * @param string $string
 *
 * @return boolean
 */
function stri_starts_with($sequence, $string)
{
    return 0 === stripos($string, $sequence);
}

/**
 * contained_and_starts_with
 *
 * @param string $sequence
 * @param array $comparable
 * @access
 * @return boolean
 */
function contained_and_starts_with(array $comparable, $string)
{
    while (count($comparable)) {
        if (true === str_starts_with(array_shift($comparable), $string)) {
            return true;
        }
    }
    return false;
}

/**
 * contained_and_ends_with
 *
 * @param array $comparable
 * @param mixed $string
 * @access
 * @return mixed
 */
function contained_and_ends_with(array $comparable, $string)
{
    while (count($comparable)) {
        if (true === str_ends_with(array_shift($comparable), $string)) {
            return true;
        }
    }
    return false;
}

/**
 * determine if a string ends wiht a given sequence
 *
 * @param string $sequence
 * @param string $string
 *
 * @return boolean
 */
function str_ends_with($sequence, $string)
{
    return (strlen($string) - strlen($sequence)) === strpos($string, $sequence);
}

/**
 * determine if a string ends wiht a given sequence
 *
 * @param string $sequence
 * @param string $string
 *
 * @return boolean
 */
function stri_ends_with($sequence, $string)
{
    return (strlen($string) - strlen($sequence)) === stripos($string, $sequence);
}

/**
 * determine if a string contains a gicen string sequence
 *
 * @param string $sequence
 * @param string $string
 *
 * @return boolean
 */
function str_contains($sequence, $string)
{
    return false !== strpos($string, $sequence);
}

/**
 * str_contains
 *
 * @param string $sequence
 * @param string $string
 *
 * @return boolean
 */
function stri_contains($sequence, $string)
{
    return false !== stripos($string, $sequence);
}

/**
 * returns the substring after the first occurance of a given character
 *
 * @param string $char
 * @param string $string
 *
 * @return string|boolean
 */
function substr_after($char, $string)
{
    return false !== ($pos = strpos($string, $char)) ? substr($string, $pos + 1) : false;
}

/**
 * returns the substring after the first occurance of a given character
 * (case insensitive)
 *
 * @param string $char
 * @param string $string
 *
 * @return string|boolean
 */
function substri_after($char, $string)
{
    return false !== ($pos = stripos($string, $char)) ? substr($string, $pos + 1) : false;
}

/**
 * returns the substring before the first occurance of a given character
 *
 * @param string $char
 * @param string $string
 *
 * @return string|boolean
 */
function substr_before($char, $string)
{
    return false !== ($pos = strpos($string, $char)) ? substr($string, 0, $pos) : false;
}

/**
 * returns the substring before the first occurance of a given character
 * (case insensitive)
 *
 * @param string $char
 * @param string $string
 *
 * @return string|boolean
 */
function substri_before($char, $string)
{
    return false !== ($pos = stripos($string, $char)) ? substr($string, 0, $pos) : false;
}

/**
 * concatenate string|number|object segments
 *
 * Note that passing an object that doesn't specify a `__toString` method
 * this will raise a runtime exception
 *
 * @param string $char
 * @param string $string
 *
 * @return string
 */
function str_concat()
{
    return vsprintf(str_repeat('%s', count($args = func_get_args())),  $args);
}
