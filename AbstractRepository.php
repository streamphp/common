<?php

/**
 * This File is part of the Stream\Common package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Common;

/**
 * @class AbstractRepository
 */

abstract class AbstractRepository
{
    /**
     * get
     *
     * @param mixed $id
     * @access public
     * @abstract
     * @return mixed
     */
    abstract public function get($id);

    /**
     * set
     *
     * @param mixed $id
     * @param mixed $value
     * @access public
     * @abstract
     * @return mixed
     */
    abstract public function set($id, $value);
}
