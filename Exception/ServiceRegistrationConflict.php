<?php

/**
 * This File is part of the Stream\Common\Exception package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Common\Exception;

/**
 * @class ServiceRegistrationConflict
 */
class ServiceRegistrationConflict extends \Exception
{
}
