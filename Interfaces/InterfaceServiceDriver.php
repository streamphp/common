<?php

/**
 * This File is part of the Stream\Common\Interfaces package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Common\Interfaces;

/**
 * interface: InterfaceServiceDriver
 *
 *
 * @package
 * @version
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
interface InterfaceServiceDriver
{
    /**
     * priority
     *
     * @access public
     * @return integer
     */
    public function priority();

    /**
     * deferred
     *
     * @access public
     * @return boolean
     */
    public function deferred();

    /**
     * register services
     *
     * @access public
     *
     * @throws Stream\Common\Exception\ServiceRegistrationConflict
     * @return void
     */
    public function register();

    /**
     * Register a packege
     *
     * @param string $name       name of the package
     * @param string $namespace  namespace of the package
     * @param string $path       location of the package
     *
     * @access public
     * @final
     * @return boolean
     */
    public function package($name, $namespace = null, $path = null);

    /**
     * provides
     *
     * @access public
     * @final
     * @return array
     */
    public function provides();

    /**
     * boot a package
     *
     * @access public
     * @return void
     */
    public function boot();
}
