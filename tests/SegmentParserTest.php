<?php

/**
 * This File is part of the Stream\Configuration\tests package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Tests\Common;

use Stream\Tests\Common\Stubs\SegmentParserStub;

/**
 * @class SegmentParserTest
 */
class SegmentParserTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ClassName
     */
    protected $parser;

    protected function setUp()
    {
        $this->parser = new SegmentParserStub();
    }

    public function segments()
    {
        return [
            [[null, 'segment', 'item'], 'segment.item'],
            [['namespace', 'segment', 'item'], 'namespace::segment.item'],
            [['namespace', 'segment', 'item'], 'namespace::segment.item.item'],
            [[null, 'segment', null], 'segment']
        ];
    }

    /**
     * @test
     * @dataProvider segments
     */
    public function testParseKeys($equals, $expression)
    {
        $segments = $this->parser->parse($expression);
        $this->assertSame($equals, $segments);
    }
}
