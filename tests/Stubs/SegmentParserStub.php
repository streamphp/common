<?php
/**
 * This File is part of the Stream\Tests Package package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */


namespace Stream\Tests\Common\Stubs;

use Stream\Common\AbstractSegmentParser;

/**
 * Class: SegmentParserStub
 *
 * @uses AbstractSegmentParser
 *
 * @package
 * @version
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
class SegmentParserStub extends AbstractSegmentParser
{}
