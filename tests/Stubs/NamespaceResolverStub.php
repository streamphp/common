<?php

/**
 * This File is part of the Stubs package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Stream\Tests\Common\Stubs;

use Stream\Common\AbstractNamespaceResolver;

/**
 * @class StubsServiceDriver
 */

class NamespaceResolverStub extends AbstractNamespaceResolver
{
}
